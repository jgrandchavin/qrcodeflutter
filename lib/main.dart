import 'package:flutter/material.dart';
import 'package:qrcodeflutter/ui/generate/GenerateQRCodeView.dart';
import 'package:qrcodeflutter/ui/history/GenerationHistoryView.dart';
import 'package:qrcodeflutter/ui/read/ReadQRCodeView.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'QRCode',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.white
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget _currentView = ReadQRCodeView();
  String _currentViewTitle = 'Read QRCode';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff2f3542),
          title: Text(_currentViewTitle),
          elevation: 0,
          ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text('QRCode Flutter'),
              ),
              ListTile(
                onTap: () {
                  setState(() {
                    _currentView = ReadQRCodeView();
                    _currentViewTitle = 'Read QRCode';
                  });
                  Navigator.pop(context);
                },
                title: Text('Read QRCode'),
              ),
              ListTile(
                onTap: () {
                  setState(() {
                    _currentView = GenerateQRCodeView();
                    _currentViewTitle = 'Generate QRCode';
                  });
                  Navigator.pop(context);
                },
                title: Text('Generate QRCode'),
              ),
              ListTile(
                onTap: () {
                  setState(() {
                    _currentView = GenerationHistoryView();
                    _currentViewTitle = 'Generation history';
                  });
                  Navigator.pop(context);
                },
                title: Text('Generation history'),
              )
            ],
          ),
        ),
        body: _currentView);
  }
}

