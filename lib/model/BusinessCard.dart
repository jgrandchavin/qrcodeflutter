class BusinessCard {
  String name;
  String url;
  String email;
  String tel;

  BusinessCard({this.name, this.url, this.email, this.tel});
}
