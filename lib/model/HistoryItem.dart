class HistoryItem {
  int id;
  String content;
  String createdAt;

  HistoryItem({this.id, this.content, this.createdAt});

  factory HistoryItem.fromMap(Map<String, dynamic> json) => new HistoryItem(id: json['id'], content: json['content'], createdAt: json['created_at']);

  Map<String, dynamic> toMap() => {
        "id": id,
        "content": content,
        "created_at": createdAt,
      };
}
