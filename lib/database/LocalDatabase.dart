import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qrcodeflutter/model/HistoryItem.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "history.db");
    return await openDatabase(path, version: 1, onOpen: (db) {}, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE History ("
          "id INTEGER PRIMARY KEY,"
          "content TEXT,"
          "created_at TEXT"
          ")");
    });
  }

  addToHistory(HistoryItem newHistoryItem) async {
    final db = await database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM History");
    int id = table.first["id"];
    var raw = await db.rawInsert(
        "INSERT Into History (id,content,created_at)"
        " VALUES (?,?,?)",
        [id, newHistoryItem.content, newHistoryItem.createdAt]);
    return raw;
  }

  Future<List<HistoryItem>> getHistory() async {
    final db = await database;
    var res = await db.query("History");
    List<HistoryItem> list = res.isNotEmpty ? res.map((c) => HistoryItem.fromMap(c)).toList() : [];
    return list;
  }
}
