import 'package:flutter/material.dart';
import 'package:qrcodeflutter/database/LocalDatabase.dart';
import 'package:qrcodeflutter/model/HistoryItem.dart';

class GenerationHistoryView extends StatefulWidget {
  @override
  _GenerationHistoryViewState createState() => _GenerationHistoryViewState();
}

class _GenerationHistoryViewState extends State<GenerationHistoryView> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: FutureBuilder<List<HistoryItem>>(
      future: DBProvider.db.getHistory(),
      builder: (BuildContext context, AsyncSnapshot<List<HistoryItem>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(
              child: Container(
                child: Text('load'),
              ),
            );
          case ConnectionState.done:
            if (snapshot.hasError) {
              return Container();
            }
            return ListView(
                children: snapshot.data.map((HistoryItem item) {
              return ListTile(
                title: Text(item.content),
                trailing: Text(
                    '${DateTime.parse(item.createdAt).day.toString()}-${DateTime.parse(item.createdAt).month.toString()}-${DateTime.parse(item.createdAt).year.toString()}'),
              );
            }).toList());
        }
      },
    ));
  }
}
