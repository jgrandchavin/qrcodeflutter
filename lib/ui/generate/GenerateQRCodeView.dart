import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qrcodeflutter/constants/ColorsConstants.dart';
import 'package:qrcodeflutter/database/LocalDatabase.dart';
import 'package:qrcodeflutter/model/HistoryItem.dart';
import 'package:share_extend/share_extend.dart';

class GenerateQRCodeView extends StatefulWidget {
  @override
  _GenerateQRCodeViewState createState() => _GenerateQRCodeViewState();
}

class _GenerateQRCodeViewState extends State<GenerateQRCodeView> {
  String _stringToPutInQRCode = '';
  GlobalKey globalKey = GlobalKey();
  bool _generated = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      margin: EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Center(
            child: RepaintBoundary(
              key: globalKey,
              child: QrImage(
                data: _stringToPutInQRCode,
                size: 200.0,
              ),
            ),
          ),
          !_generated
              ? TextField(
                  onChanged: (value) {
                    setState(() {
                      _stringToPutInQRCode = value;
                    });
                  },
                  decoration: InputDecoration(hintText: 'QRCode value'),
                )
              : Container(),
          !_generated
              ? FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6), side: BorderSide(color: ColorsConstants.GREY, width: 1)),
                  onPressed: () async {
                    if (_stringToPutInQRCode.isNotEmpty) {
                      print(_stringToPutInQRCode);
                      await DBProvider.db.addToHistory(HistoryItem(createdAt: DateTime.now().toString(), content: _stringToPutInQRCode));
                      setState(() {
                        _generated = true;
                      });
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return Dialog(
                              child: Container(padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20), child: Text('Enter a value!')),
                            );
                          });
                    }
                  },
                  child: Text('Generate'),
                )
              : Container(),
          _generated
              ? FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6), side: BorderSide(color: ColorsConstants.GREY, width: 1)),
                  onPressed: _captureAndSharePng,
                  child: Text('Share'),
                )
              : Container(),
          _generated
              ? FlatButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6), side: BorderSide(color: ColorsConstants.GREY, width: 1)),
                  onPressed: () {
                    setState(() {
                      _generated = false;
                    });
                  },
                  child: Text('Restart'),
                )
              : Container()
        ],
      ),
    );
  }

  Future<void> _captureAndSharePng() async {
    try {
      RenderRepaintBoundary boundary = globalKey.currentContext.findRenderObject();
      var image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();

      final tempDir = await getTemporaryDirectory();
      final file = await new File('${tempDir.path}/image.png').create();
      await file.writeAsBytes(pngBytes);

      ShareExtend.share('${tempDir.path}/image.png', 'image');
    } catch (e) {
      print(e.toString());
    }
  }
}
