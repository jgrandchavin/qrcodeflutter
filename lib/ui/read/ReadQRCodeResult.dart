import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:qrcodeflutter/constants/ColorsConstants.dart';
import 'package:qrcodeflutter/model/BusinessCard.dart';

class ReadQRCodeResult extends StatefulWidget {
  final String result;

  ReadQRCodeResult({@required this.result});

  @override
  _ReadQRCodeResultState createState() => _ReadQRCodeResultState();
}

class _ReadQRCodeResultState extends State<ReadQRCodeResult> {
  bool _isBusinessCard = false;
  BusinessCard _businessCard;

  @override
  void initState() {
    if (widget.result.startsWith('BEGIN:VCARD')) {
      setState(() {
        _isBusinessCard = true;
      });

      List<String> businessCardFromString = widget.result.split('\n');
      String name = businessCardFromString[1].replaceAll('N:', '').replaceAll(';', ' ');
      String url = businessCardFromString[2].replaceAll('URL:', '');
      String email = businessCardFromString[3].replaceAll('EMAIL:', '');
      String tel = businessCardFromString[4].replaceAll('TEL:', '');

      setState(() {
        _businessCard = BusinessCard(name: name, email: email, tel: tel, url: url);
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Result',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 25),
              ),
              SizedBox(height: 50),
              _isBusinessCard
                  ? BusinessCardWidget(
                      businessCard: _businessCard,
                    )
                  : Container(
                      decoration: BoxDecoration(color: ColorsConstants.GREY, borderRadius: BorderRadius.circular(10)),
                      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      child: Text(
                        widget.result,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
              SizedBox(height: 20),
              FlatButton(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(6)), side: BorderSide()),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Return'),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class BusinessCardWidget extends StatelessWidget {
  final BusinessCard businessCard;

  BusinessCardWidget({this.businessCard});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: ColorsConstants.GREY),
      child: Row(
        children: <Widget>[
          SvgPicture.asset(
            'assets/boss.svg',
            height: 100,
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                businessCard.name,
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 20),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                businessCard.email,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                businessCard.url,
                style: TextStyle(color: Colors.white),
              ),
              Text(
                businessCard.tel,
                style: TextStyle(color: Colors.white),
              )
            ],
          )
        ],
      ),
    );
  }
}
