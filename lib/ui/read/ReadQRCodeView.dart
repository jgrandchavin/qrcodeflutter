import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:qrcodeflutter/ui/read/ReadQRCodeResult.dart';

class ReadQRCodeView extends StatefulWidget {
  @override
  _ReadQRCodeViewState createState() => _ReadQRCodeViewState();
}

class _ReadQRCodeViewState extends State<ReadQRCodeView> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        InkWell(
          onTap: _scanFromCamera,
          child: Column(
            children: <Widget>[
              SvgPicture.asset(
                'assets/photo-camera.svg',
                height: 100,
              ),
              Text(
                'Read from camera',
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        SizedBox(
          height: 50,
        ),
        InkWell(
          onTap: _readFromImage,
          child: Column(
            children: <Widget>[
              SvgPicture.asset(
                'assets/archive.svg',
                height: 100,
              ),
              Text(
                'Read from file',
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ],
    ));
  }

  Future _readFromImage() async {
    await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReadQRCodeResult(
        result: 'Don\'t find how read QRCode from file with flutter...',
      );
    }));
  }

  Future _scanFromCamera() async {
    String result = await QRCodeReader()
        .setAutoFocusIntervalInMs(200) // default 5000
        .setForceAutoFocus(true) // default false
        .setTorchEnabled(true) // default false
        .setHandlePermissions(true) // default true
        .setExecuteAfterPermissionGranted(true) // default true
        .scan();

    if (result != null) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ReadQRCodeResult(
          result: result,
        );
      }));
    }
  }
}
